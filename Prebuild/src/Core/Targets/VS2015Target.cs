﻿using System;
using System.IO;
using System.Text;

using Prebuild.Core.Attributes;
using Prebuild.Core.Interfaces;
using Prebuild.Core.Nodes;
using Prebuild.Core.Utilities;
using System.CodeDom.Compiler;

namespace Prebuild.Core.Targets
{

    /// <summary>
    /// 
    /// </summary>
    [Target("vs2015")]
    public class VS2015Target : VSGenericTarget
    {
        #region Fields

        string solutionVersion = "14.00";

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the solution version.
        /// </summary>
        /// <value>The solution version.</value>
        public override string SolutionVersion => solutionVersion;

        /// <summary>
        /// Gets or sets the product version.
        /// </summary>
        /// <value>The product version.</value>
        public override string ProductVersion { get; } = "14.0.25123";

        /// <summary>
        /// Gets or sets the schema version.
        /// </summary>
        /// <value>The schema version.</value>
        public override string SchemaVersion { get; } = "2.0";

        /// <summary>
        /// Gets or sets the name of the version.
        /// </summary>
        /// <value>The name of the version.</value>
        public override string VersionName { get; } = "Visual Studio 2015";

        /// <summary>
        /// Gets or sets the version.
        /// </summary>
        /// <value>The version.</value>
        public override VSVersion Version { get; } = VSVersion.VS14;

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>The name.</value>
        public override string Name { get; } = "vs2015";

        protected override string GetToolsVersionXml(FrameworkVersion frameworkVersion)
        {
            switch (frameworkVersion)
            {
                case FrameworkVersion.v4_6_2:
                case FrameworkVersion.v4_6_1:
                case FrameworkVersion.v4_6:
                    return "ToolsVersion=\"12.0\"";
                case FrameworkVersion.v4_5_2:
                case FrameworkVersion.v4_5_1:
                case FrameworkVersion.v4_5:
                case FrameworkVersion.v4_0:
                case FrameworkVersion.v3_5:
                    return "ToolsVersion=\"4.0\"";
                case FrameworkVersion.v3_0:
                    return "ToolsVersion=\"3.0\"";
                default:
                    return "ToolsVersion=\"2.0\"";
            }
        }

        public override string SolutionTag => "# Visual Studio 2015";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VS2005Target"/> class.
        /// </summary>
        public VS2015Target()
            : base()
        {
        }

        #endregion
    }
}

